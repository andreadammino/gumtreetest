export default class Utils {

	static e(element,className){
		const el = document.createElement(element);
		(className)? el.className+= ` ${className}` : '';
		
		return el;
	}

	static byId(element){
		return document.getElementById(element);
	}

	static byClassName(className){
		return document.getElementsByClassName(className);
	}

	static removeEl (elClass){
		this.byClassName(elClass)[0].parentNode.removeChild(this.byClassName(elClass)[0]);
	}
}
