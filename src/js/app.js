require('../css/main.scss');

import Game from './components/game';
import GUI from './components/GUI';
import Utils from '../lib/utils';

export default class App {

	static init(mainContainer){
		this.mainContainer = Utils.byId(mainContainer);
		Game.play('PAPER',Game.getRandomChoice());
		const gui = new GUI();
		this.mainContainer.appendChild(gui.render('player','computer'));
	}
}

App.init('app');

