import Game from '../components/game';
import Utils from '../../lib/utils';

export default class Events {

	static playGame (gui) {
		const randChoice = Game.getRandomChoice();
		const player1= Utils.byClassName('player1')[0].innerHTML;

		if(!this.choice && player1=='player'){
			this.updateGUI(gui,'','','Please select Rock Paper or Scissors!');
			return;
		}

		if(this.choice && player1=='player'){
			const res = Game.play(this.choice,randChoice);
			this.updateGUI(gui,this.choice,randChoice,res);
			return;
		}

		if( player1=='computer'){
			const choice1=Game.getRandomChoice();
			const res = Game.play(choice1,randChoice);
			this.updateGUI(gui,choice1,randChoice,res);
			return;
		}
		
		this.choice = null;
	}

	static restartGame (gui) {
		this.updateGUI(gui);
		this.choice = null;
	}

	static selectChoice (choice,gui){
		this.choice=choice;
		this.updateGUI(gui,this.choice);	
	}

	static updateGUI(gui,choice='',randChoice='',score=''){
		const main=gui.main;
		Utils.removeEl('gameScoreContainer');
		main.appendChild(gui.gameScoreContainer().render(choice,randChoice,score));
	}

	static swapPlayerComputer(pl,gui){
		const app = Utils.byId('app');
		
		if(pl=='player'){
			Utils.removeEl('main');
			app.appendChild(gui.render('player','computer'));
		}else if(pl=='computer'){
			Utils.removeEl('choiceContainer');
			this.updateGUI(gui);
		}
	}
}