import Utils from '../../lib/utils';

export default class Button {
	constructor(label, type = 'button'){
		this.label=label;
		this.type=type;
	}

	render(){
		const b = Utils.e('button');
		b.type=this.type;
		b.innerHTML=this.label;
		
		return b;
	}
}