import Utils from '../../lib/utils';
import Button from './button';
import Events from './events';
import Choices from '../constants/choices';

export default class GUI {

	render(...labels){
		this.main= Utils.e('div','main');

		const playerContainer = this.playerContainer();
		const choiceContainer = this.choiceContainer();
		const gameModeContainer = this.gameModeContainer();
		const gameScoreContainer = this.gameScoreContainer();

		this.main.appendChild(playerContainer.render(labels));
		this.main.appendChild(choiceContainer.render(Choices.choices));
		this.main.appendChild(gameModeContainer.render());
		this.main.appendChild(gameScoreContainer.render());

		return this.main;
	}

	playerContainer(){

		const playerContainer= Utils.e('div','playerContainer');

		const player = (label,className) => {
			let pl= Utils.e('div',className);
			pl.innerHTML= label;
			if(className=='player1'){
				pl.addEventListener('click', () => {
					pl.innerHTML = (pl.innerHTML=='player')? 'computer' : 'player';
					Events.swapPlayerComputer(pl.innerHTML,this);
				});
			}
			
			return pl;
		};

		const render = (labels) => {
			let i =1;
			labels.map((l) => {
				const p = new player(l,'player'+ i++);
				playerContainer.appendChild(p);
			});
			i=1;
			return playerContainer;
		};
		
		return {
			render:render
		}
	}

	choiceContainer(){
		const choiceContainer= Utils.e('div','choiceContainer');

		const choice = (label) =>{
			const b = new Button(label);
			const renderedButton =b.render();
			renderedButton.addEventListener('click', () => Events.selectChoice(b.label,this));
			
			return renderedButton;
		};

		const render = (labels) => {

			labels.map((l) => {
				const c = new choice(l);
				choiceContainer.appendChild(c);


			});
			return choiceContainer;
		};

		return {
			render:render
		}
	}

	gameModeContainer(){

		const gameModeContainer= Utils.e('div','gameModeContainer');
		
		const playButton = new Button('Play');
		const restartButton = new Button('Restart');
		const renderPlay = playButton.render();
		const restartPlay = restartButton.render();

		const render = () => {
			gameModeContainer.appendChild(renderPlay);
			gameModeContainer.appendChild(restartPlay);

			return gameModeContainer;
		}

		renderPlay.addEventListener('click', () => Events.playGame(this));
		restartPlay.addEventListener('click', () => Events.restartGame(this));

		return {
			render:render
		}

	}

	gameScoreContainer(){
		const gameScoreContainer = Utils.e('div','gameScoreContainer');
		const p1Choice = Utils.e('div','p1');
		const p2Choice = Utils.e('div','p2');
		const gameScore= Utils.e('div','gameScore');

		const render = (p1 ='', p2 ='',score ='') => {
			p1Choice.innerHTML=p1;
			p2Choice.innerHTML=p2;
			gameScore.innerHTML=score;

			gameScoreContainer.appendChild(p1Choice);
			gameScoreContainer.appendChild(p2Choice);
			gameScoreContainer.appendChild(gameScore);

			return gameScoreContainer;
		}

		return {
			render:render
		}
	}
}

