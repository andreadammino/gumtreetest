import Game from '../src/js/components/game.js';


describe('module Game', () => {
	
	it('should return draw if same choices', () => {
		expect(Game.play('SCISSORS', 'SCISSORS')).toBe('DRAW');
		expect(Game.play('PAPER', 'PAPER')).toBe('DRAW');
		expect(Game.play('ROCK', 'ROCK')).toBe('DRAW');
	});

	it('should return win', () => {
		expect(Game.play('SCISSORS', 'PAPER')).toBe('WINNER');
		expect(Game.play('PAPER', 'ROCK')).toBe('WINNER');
		expect(Game.play('ROCK', 'SCISSORS')).toBe('WINNER');
	});

	it('should return lose', () => {
		expect(Game.play('SCISSORS', 'ROCK')).toBe('LOSER');
		expect(Game.play('PAPER', 'SCISSORS')).toBe('LOSER');
		expect(Game.play('ROCK', 'PAPER')).toBe('LOSER');
	});

	it('should get a random choice', () => {
		expect(Game.getRandomChoice()).toBeDefined();
		expect(Game.getRandomChoice()).toBeDefined();
		expect(Game.getRandomChoice()).toBeDefined();
	});
});
